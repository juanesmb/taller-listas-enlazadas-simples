package ufps.util.colecciones_seed;

import Modelo.Estudiante;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author juane
 */
public class ListaSTest {
    
    public ListaSTest() {
    }

    @org.junit.jupiter.api.Test
    public void testInsertarAlFinal1() 
    {
        System.out.println("Prueba #1 - insertar al final");
        //creación de escenario
        ListaS l = new ListaS();
        Estudiante a = new Estudiante(11,"alvaro");
        Estudiante b = new Estudiante(5,"bacily");
        Estudiante c = new Estudiante(0,"camila");
        System.out.println("estudiantes creados");
        
        l.insertarAlFinal(a);
        l.insertarAlFinal(b);
        l.insertarAlFinal(c);
        System.out.println("estudiantes insertados");
        System.out.println(l.toString());
        Object [] v = l.aVector();
        
        //lo esperado
        boolean paso = false;
        Nodo<Estudiante> aux = l.getCabeza();
        
        for (int i = 0; i < v.length; i++) 
        {
            if(v[i].equals(aux.getInfo()))
                paso = true;
            aux = aux.getSig();
        }
        assertTrue(paso);
        System.out.println("");
    }
    
    @org.junit.jupiter.api.Test
    public void testInsertarAlFinal2() 
    {
        System.out.println("Prueba #2 - insertar al final");
        //creación de escenario
        ListaS l = new ListaS();
        Estudiante a = new Estudiante(11,"alvaro");
        Estudiante d = new Estudiante(15,"david");
        Estudiante e = new Estudiante(8,"eliana");
        System.out.println("estudiantes creados");
        
        l.insertarAlFinal(a);
        l.insertarAlFinal(d);
        l.insertarAlFinal(e);
        System.out.println("estudiantes insertados");
        System.out.println(l.toString());
        Object [] v = l.aVector();
        
        //lo esperado
        boolean paso = false;
        Nodo<Estudiante> aux = l.getCabeza();
        
        for (int i = 0; i < v.length; i++) 
        {
            if(v[i].equals(aux.getInfo()))
                paso = true;
            aux = aux.getSig();
        }
        assertTrue(paso);
        System.out.println("");
    }
    
    @org.junit.jupiter.api.Test
    public void testOrdenarSeleccion_Por_Nodos1() 
    {
        System.out.println("Prueba#1 - ordenar por selección_nodos");
        //creación de escenario
        ListaS l = new ListaS();
        Estudiante a = new Estudiante(11,"alvaro");
        Estudiante b = new Estudiante(5,"bacily");
        Estudiante c = new Estudiante(0,"camila");
        Estudiante d = new Estudiante(15,"david");
        Estudiante e = new Estudiante(8,"eliana");
        System.out.println("estudiantes creados");
        
        l.insertarAlFinal(a);
        l.insertarAlFinal(b);
        l.insertarAlFinal(c);
        l.insertarAlFinal(d);
        l.insertarAlFinal(e);
        System.out.println("estudiantes insertados");
        Object [] v = l.aVector();
        
        l.ordenarSeleccion_Por_Nodos();
        System.out.println("estudiantes ordenados");
        System.out.println(l.toString());
        
        //lo esperado
        boolean paso = false;
        
        
        Nodo<Estudiante> aux = l.getCabeza();
        
        for (int i = 0; i < v.length; i++) 
        {
            if(v[i].equals(aux.getInfo()))
                paso = true;
            aux = aux.getSig();
        }
        assertTrue(paso);
        System.out.println("");
    }
    
    @org.junit.jupiter.api.Test
    public void testOrdenarSeleccion_Por_Nodos2() 
    {
        System.out.println("Prueba#2 - ordenar por selección_nodos");
        //creación de escenario
        ListaS l = new ListaS();
        Estudiante a = new Estudiante(50,"alvaro");
        Estudiante b = new Estudiante(2000,"bacily");
        Estudiante c = new Estudiante(20000,"camila");
        Estudiante d = new Estudiante(-50,"david");
        Estudiante e = new Estudiante(0,"eliana");
        System.out.println("estudiantes creados");
        
        l.insertarAlFinal(a);
        l.insertarAlFinal(b);
        l.insertarAlFinal(c);
        l.insertarAlFinal(d);
        l.insertarAlFinal(e);
        System.out.println("estudiantes insertados");
        Object [] v = l.aVector();
        
        l.ordenarSeleccion_Por_Nodos();
        System.out.println("estudiantes ordenados");
        System.out.println(l.toString());
        
        //lo esperado
        boolean paso = false;
        
        
        Nodo<Estudiante> aux = l.getCabeza();
        
        for (int i = 0; i < v.length; i++) 
        {
            if(v[i].equals(aux.getInfo()))
                paso = true;
            aux = aux.getSig();
        }
        assertTrue(paso);
        System.out.println("");
    }

    @org.junit.jupiter.api.Test
    public void testSet1() 
    {
        System.out.println("Prueba #1 - set");
        //creación de escenario
        ListaS l = new ListaS();
        Estudiante a = new Estudiante(11,"alvaro");
        Estudiante b = new Estudiante(5,"bacily");
        Estudiante c = new Estudiante(0,"camila");
        
        Estudiante d = new Estudiante(15,"david");
        Estudiante e = new Estudiante(8,"eliana");
        System.out.println("estudiantes creados");
        
        l.insertarAlFinal(a);
        l.insertarAlFinal(b);
        l.insertarAlFinal(c);
        System.out.println("estudiantes insertados");
        System.out.println(l.toString());
        
        l.set(0, d);
        l.set(2, e);
        Object [] v = l.aVector();
        System.out.println(l.toString());
        
        //lo esperado
        boolean paso = false;
        if(v[0].equals(d) && v[2].equals(e))
            paso = true;
        assertTrue(paso);
        System.out.println("");
    }
    
    public void testSet2() 
    {
        System.out.println("Prueba #2 - set");
        //creación de escenario
        ListaS l = new ListaS();
        Estudiante a = new Estudiante(11,"alvaro");
        Estudiante b = new Estudiante(5,"bacily");
        Estudiante c = new Estudiante(0,"camila");
        
        Estudiante d = new Estudiante(15,"david");
        Estudiante e = new Estudiante(8,"eliana");
        System.out.println("estudiantes creados");
        
        l.insertarAlFinal(d);
        l.insertarAlFinal(e);
        l.insertarAlFinal(c);
        System.out.println("estudiantes insertados");
        System.out.println(l.toString());
        
        l.set(0, a);
        l.set(1, b);
        Object [] v = l.aVector();
        System.out.println(l.toString());
        
        //lo esperado
        boolean paso = false;
        if(v[0].equals(a) && v[1].equals(b))
            paso = true;
        assertTrue(paso);
        System.out.println("");
    }
    
    public void testInsertarOrdenado1() 
    {
        System.out.println("Prueba #1 - insertar ordenado");
        //creación de escenario
        ListaS l = new ListaS();
        Estudiante a = new Estudiante(0,"alvaro");
        Estudiante b = new Estudiante(5,"bacily");
        Estudiante c = new Estudiante(50,"camila");
        Estudiante d = new Estudiante(100,"david");
        Estudiante e = new Estudiante(5000,"eliana");
        System.out.println("estudiantes creados");
        
        l.insertarAlFinal(a);
        l.insertarAlFinal(b);
        l.insertarAlFinal(d);
        l.insertarAlFinal(e);
        System.out.println("estudiantes insertados");
        System.out.println(l.toString());
        
        l.insertarOrdenado(c);
        
        Object [] v = l.aVector();
        System.out.println(l.toString());
        
        //lo esperado
        boolean paso = false;
        if(v[2].equals(c))
            paso = true;
        assertTrue(paso);
        System.out.println("");
    }
    
    public void testInsertarOrdenado2() 
    {
        System.out.println("Prueba #2 - insertar ordenado");
        //creación de escenario
        ListaS l = new ListaS();
        Estudiante a = new Estudiante(0,"alvaro");
        Estudiante b = new Estudiante(5,"bacily");
        Estudiante c = new Estudiante(50,"camila");
        Estudiante d = new Estudiante(100,"david");
        Estudiante e = new Estudiante(5000,"eliana");
        System.out.println("estudiantes creados");
        
        
        l.insertarAlFinal(b);
        l.insertarAlFinal(c);
        l.insertarAlFinal(d);
        l.insertarAlFinal(e);
        System.out.println("estudiantes insertados");
        System.out.println(l.toString());
        
        l.insertarOrdenado(a);
        
        Object [] v = l.aVector();
        System.out.println(l.toString());
        
        //lo esperado
        boolean paso = false;
        if(v[0].equals(a))
            paso = true;
        assertTrue(paso);
        System.out.println("");
    }

    

    

   

    
    
}
