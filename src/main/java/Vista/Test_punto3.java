/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Modelo.Estudiante;
import java.util.Scanner;
import ufps.util.colecciones_seed.ListaS;

/**
 *
 * @author juane
 */
public class Test_punto3 {
    
    public static void main(String[] args) {
        
        while(true)
        {
            //CREACIÓN DEL ESCENARIO
            ListaS l = new ListaS();
            Estudiante a = new Estudiante(11,"alvaro");
            Estudiante b = new Estudiante(5,"bacily");
            Estudiante c = new Estudiante(0,"camila");
            Estudiante d = new Estudiante(15,"david");
            Estudiante e = new Estudiante(8,"eliana");
            System.out.println("INICIO ----------------------- " + "\n" + "estudiantes creados");

            l.insertarAlFinal(a);
            l.insertarAlFinal(b);
            l.insertarAlFinal(c);
            l.insertarAlFinal(d);
            l.insertarAlFinal(e);

            System.out.println(l.toString()+"\n");
            ////////////////////////////////////////////////////////////

            //INTERFAZ
            Scanner sc = new Scanner (System.in);
            System.out.println("1.Ordenar lista por movimiento de nodos ");
            System.out.println("2.Ordenar lista por movimiento infos");
            System.out.println("3. terminar programa");
            int n = sc.nextInt();
            ////////////////////////////////////////////////////
            
            try
            {
                if(n == 1)
                {
                    l.ordenarSeleccion_Por_Nodos();
                    System.out.println("estudiantes ordenados");
                    //System.out.println(l.toString()+"\n");
                }
                if(n == 2)
                {
                    l.ordenarSeleccion_Por_Infos();
                    System.out.println("estudiantes ordenados: ");
                    System.out.println(l.toString()+"\n");
                }
                if(n == 3)
                {
                    break;
                }
                if(n!=1 && n!=2 && n!=3)
                {
                    System.err.println("número incorrecto.");
                }
            }catch(Exception f)
            {
                System.err.println(f.getMessage());
            }
            
        }
    }
    
}
