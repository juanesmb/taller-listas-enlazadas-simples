package Vista;

import java.util.Random;
import ufps.util.colecciones_seed.ListaS;

/**
 *
 * @author juan Moncada
 */
public class Experimento {
    
    public static void main(String[] args) {
        
        //experimento1();
        //experimento2();
        //experimento3();
        //experimento4();
        //experimento5();
        experimento6();
        //prueba();
        
        
    }
    
    //Crea una LISTA de números de 1 a 20000 y calcule el tiempo de invocar el método de selección.
    private static void experimento1() {
        ListaS<Integer> info = new ListaS();
        int i = 20000;
        while(i>0){
            info.insertarAlFinal(i);
            i--;
        }
        //CALCULAMOS TIEMPO
        long TInicioInfo, TFinInfo, tiempoInfo; 
        TInicioInfo = System.currentTimeMillis();
        try{
            info.ordenarSeleccion_Por_Infos();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        TFinInfo = System.currentTimeMillis();
        tiempoInfo = (TFinInfo - TInicioInfo);
        System.out.println("info - tiempo de ejecución: " + tiempoInfo + " miliSegundos");
        
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        ListaS<Integer> nodo = new ListaS();
        int j = 20000;
        while(j>0){
            nodo.insertarAlFinal(j);
            j--;
        }
        //CALCULAMOS TIEMPO
        long TInicioNodo, TFinNodo, tiempoNodo; 
        TInicioNodo = System.currentTimeMillis();
        try{
            nodo.ordenarSeleccion_Por_Nodos();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        TFinNodo = System.currentTimeMillis();
        tiempoNodo = (TFinNodo - TInicioNodo);
        System.out.println("Nodo - tiempo de ejecución: " + tiempoNodo + " miliSegundos");
    }

    private static void experimento2() {
        ListaS<Integer> info = new ListaS();
        int i = 200000;
        while(i>0){
            info.insertarAlFinal(i);
            i--;
        }
        //CALCULAMOS TIEMPO
        long TInicioInfo, TFinInfo, tiempoInfo; 
        TInicioInfo = System.currentTimeMillis();
        try{
            info.ordenarSeleccion_Por_Infos();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        TFinInfo = System.currentTimeMillis();
        tiempoInfo = (TFinInfo - TInicioInfo);
        System.out.println("info - tiempo de ejecución: " + tiempoInfo + " miliSegundos");
        
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        ListaS<Integer> nodo = new ListaS();
        int j = 20000;
        while(j>0){
            nodo.insertarAlFinal(j);
            j--;
        }
        //CALCULAMOS TIEMPO
        long TInicioNodo, TFinNodo, tiempoNodo; 
        TInicioNodo = System.currentTimeMillis();
        try{
            nodo.ordenarSeleccion_Por_Nodos();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        TFinNodo = System.currentTimeMillis();
        tiempoNodo = (TFinNodo - TInicioNodo);
        System.out.println("Nodo - tiempo de ejecución: " + tiempoNodo + " miliSegundos");
    }
    
    private static void experimento3() {
        ListaS<Integer> info = new ListaS();
        int i = 5000000;
        while(i>0){
            info.insertarAlFinal(i);
            i--;
        }
        //CALCULAMOS TIEMPO
        long TInicioInfo, TFinInfo, tiempoInfo; 
        TInicioInfo = System.currentTimeMillis();
        try{
            info.ordenarSeleccion_Por_Infos();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        TFinInfo = System.currentTimeMillis();
        tiempoInfo = (TFinInfo - TInicioInfo);
        System.out.println("info - tiempo de ejecución: " + tiempoInfo + " miliSegundos");
        
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        ListaS<Integer> nodo = new ListaS();
        int j = 20000;
        while(j>0){
            nodo.insertarAlFinal(j);
            j--;
        }
        //CALCULAMOS TIEMPO
        long TInicioNodo, TFinNodo, tiempoNodo; 
        TInicioNodo = System.currentTimeMillis();
        try{
            nodo.ordenarSeleccion_Por_Nodos();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        TFinNodo = System.currentTimeMillis();
        tiempoNodo = (TFinNodo - TInicioNodo);
        System.out.println("Nodo - tiempo de ejecución: " + tiempoNodo + " miliSegundos");
    }

    private static void experimento4() {
        Random r = new Random();
        ListaS<Integer> info = new ListaS();
        
         
        int i = 20000;
        while(i>0){
            int valorDadoInfo = r.nextInt(200000)+1;
            info.insertarAlFinal(valorDadoInfo);
            i--;
        }
        //CALCULAMOS TIEMPO
        long TInicioInfo, TFinInfo, tiempoInfo; 
        TInicioInfo = System.currentTimeMillis();
        try{
            info.ordenarSeleccion_Por_Infos();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        TFinInfo = System.currentTimeMillis();
        tiempoInfo = (TFinInfo - TInicioInfo);
        System.out.println("info - tiempo de ejecución: " + tiempoInfo + " miliSegundos");
        
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        ListaS<Integer> nodo = new ListaS();
        int j = 20000;
        while(j>0){
            int valorDadoNodo = r.nextInt(200000)+1;
            nodo.insertarAlFinal(valorDadoNodo);
            j--;
        }
        //CALCULAMOS TIEMPO
        long TInicioNodo, TFinNodo, tiempoNodo; 
        TInicioNodo = System.currentTimeMillis();
        try{
            nodo.ordenarSeleccion_Por_Nodos();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        TFinNodo = System.currentTimeMillis();
        tiempoNodo = (TFinNodo - TInicioNodo);
        System.out.println("Nodo - tiempo de ejecución: " + tiempoNodo + " miliSegundos");
    }

    private static void experimento5() {
        Random r = new Random();
        ListaS<Integer> info = new ListaS();
       
         
        int i = 200000;
        while(i>0){
            int valorDadoInfo = r.nextInt(2000000)+1;
            info.insertarAlFinal(valorDadoInfo);
            i--;
        }
        //CALCULAMOS TIEMPO
        long TInicioInfo, TFinInfo, tiempoInfo; 
        TInicioInfo = System.currentTimeMillis();
        try{
            info.ordenarSeleccion_Por_Infos();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        TFinInfo = System.currentTimeMillis();
        tiempoInfo = (TFinInfo - TInicioInfo);
        System.out.println("info - tiempo de ejecución: " + tiempoInfo + " miliSegundos");
        
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        ListaS<Integer> nodo = new ListaS();
        int j = 20000;
        while(j>0)
        {
            int valorDadoNodo = r.nextInt(200000)+1;
            nodo.insertarAlFinal(valorDadoNodo);
            j--;
        }
        //CALCULAMOS TIEMPO
        long TInicioNodo, TFinNodo, tiempoNodo; 
        TInicioNodo = System.currentTimeMillis();
        try{
            nodo.ordenarSeleccion_Por_Nodos();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        TFinNodo = System.currentTimeMillis();
        tiempoNodo = (TFinNodo - TInicioNodo);
        System.out.println("Nodo - tiempo de ejecución: " + tiempoNodo + " miliSegundos");
    }

    private static void experimento6() {
        Random r = new Random();
        ListaS<Integer> info = new ListaS();
         
        int i = 5000000;
        while(i>0){
            int valorDado = r.nextInt(50000000)+1;
            info.insertarAlFinal(valorDado);
            i--;
        }
        //CALCULAMOS TIEMPO
        long TInicioInfo, TFinInfo, tiempoInfo; 
        TInicioInfo = System.currentTimeMillis();
        try{
            info.ordenarSeleccion_Por_Infos();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        TFinInfo = System.currentTimeMillis();
        tiempoInfo = (TFinInfo - TInicioInfo);
        System.out.println("info - tiempo de ejecución: " + tiempoInfo + " miliSegundos");
        
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        ListaS<Integer> nodo = new ListaS();
        int j = 5000000;
        while(j>0)
        {
            int valorDadoNodo = r.nextInt(50000000)+1;
            nodo.insertarAlFinal(valorDadoNodo);
            j--;
        }
        //CALCULAMOS TIEMPO
        long TInicioNodo, TFinNodo, tiempoNodo; 
        TInicioNodo = System.currentTimeMillis();
        try{
            nodo.ordenarSeleccion_Por_Nodos();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        TFinNodo = System.currentTimeMillis();
        tiempoNodo = (TFinNodo - TInicioNodo);
        System.out.println("Nodo - tiempo de ejecución: " + tiempoNodo + " miliSegundos");
    }

    
}
